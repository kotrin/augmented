Sequel.migration do
  change do
    create_table(:handshake_requests) do
      primary_key :id
      String :name
      String :url
      String :token
      Time :approved_at
    end
  end
end
