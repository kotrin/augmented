Sequel.migration do
  up do
    add_column :handshake_requests, :failed_at, Time
  end
  down do
    drop_column :handshake_requests, :failed_at
  end
end
