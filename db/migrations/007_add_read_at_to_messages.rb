Sequel.migration do
  up do
    add_column :messages, :read_at, Time, :default => nil
  end
  down do
    drop_column :messages, :read_at
  end
end
