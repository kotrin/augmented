Sequel.migration do
  change do
    create_table(:contacts) do
      primary_key :id
      String :name
      String :token
      String :url
    end
  end
end
