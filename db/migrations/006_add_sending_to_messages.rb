Sequel.migration do
  up do
    add_column :messages, :sending, FalseClass, :default => false
  end
  down do
    drop_column :messages, :sending
  end
end
