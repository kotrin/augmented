Sequel.migration do
  up do
    add_column :handshake_requests, :sending, FalseClass, :default => false
  end
  down do
    drop_column :handshake_requests, :sending
  end
end
