Sequel.migration do
  up do
    add_column :messages, :failed_at, Time
  end
  down do
    drop_column :messages, :failed_at
  end
end
