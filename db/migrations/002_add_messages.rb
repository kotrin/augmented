Sequel.migration do
  change do
    create_table(:messages) do
      primary_key :id
      String :body
      foreign_key :contact_id, :contacts
    end
  end
end
