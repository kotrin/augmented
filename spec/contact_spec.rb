require_relative './spec_helper'

describe Augmented::Contact do
  it "requires a name" do
    contact = Augmented::Contact.new(name: "")
    contact.valid?.should == false
    contact.errors[:name].should == ["is not present"]
  end

  it "requires a token" do
    contact = Augmented::Contact.new(token: "")
    contact.valid?.should == false
    contact.errors[:token].should == ["is not present"]
  end

  it "has many messages" do
    contact = Augmented::Contact.create(name: "test", token: "123")
    contact.messages.count.should == 0
    contact.add_message(body: "test message")
    contact.messages.count.should == 1
  end
end
