require_relative './spec_helper'

describe Augmented::Message do
  it "requires a body" do
    message = Augmented::Message.new(body: "")
    expect(message.valid?).to be(false)
    expect(message.errors[:body]).to eq(["is not present"])
  end

  it "has one contact" do
    contact = Augmented::Contact.create(name: "test", token: "we123")
    message = Augmented::Message.create(body: "apple", contact_id: contact.id)
    expect(message.contact).to eq(contact)
  end  
  
  it "can be flagged as sending" do
    message = Augmented::Message.new
    message.sending = true
    expect(message.sending?).to be(true)
  end

  context "formatted_body" do
    it "returns formatted markdown" do
      body = "<test>\n\nhi! lawl"
      contact = Augmented::Contact.create(name: "bob", token: "123s", url: "appletest.com")
      message = Augmented::Message.create(body: body, contact_id: contact.id)
      expect(message.formatted_body).to eq("&lt;test&gt;\n\nhi! lawl")
    end
  end
   
  context "send_message!" do
    it "does nothing if the message is not a sending message" do
      message = Augmented::Message.new
      expect(message.send_message!).to be_nil
    end

    it "sends a http POST to the contact url /message.json" do
      contact = Augmented::Contact.create(name: "bob", token: "123s", url: "appletest.com")
      message = Augmented::Message.create(body: "hola!", contact_id: contact.id, sending: true)
      
      message_success_stub("appletest.com")
      
      message.send_message!
      expect(WebMock).to have_requested(:post, "appletest.com/message.json").with(body: { token: "123s", body: "hola!" })
    end

    context "when response is an error" do
      it "sets failed_at to current time" do
        Timecop.freeze(Time.local(2012, 10, 10)) do
          contact = Augmented::Contact.create(name: "bob", token: "123s", url: "appletest.com")
          message = Augmented::Message.create(body: "hola!", contact_id: contact.id, sending: true)
          message_error_stub("appletest.com")
          message.send_message!
          expect(message.failed_at).to eq(Time.now)
        end
      end
    end
  end
end
