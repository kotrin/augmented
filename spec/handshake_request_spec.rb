require_relative './spec_helper'

describe Augmented::HandshakeRequest do
  it "requires a token" do
    handshake = Augmented::HandshakeRequest.new(token: "")
    expect(handshake.valid?).to be(false)
    expect(handshake.errors[:token]).to eq(["is not present"])
  end

  it "requires a name" do
    handshake = Augmented::HandshakeRequest.new(name: "")
    expect(handshake.valid?).to be(false)
    expect(handshake.errors[:name]).to eq(["is not present"])
  end

  it "requires a url" do
    handshake = Augmented::HandshakeRequest.new(url: "")
    expect(handshake.valid?).to be(false)
    expect(handshake.errors[:url]).to eq(["is not present"])
  end

  it "can be flagged as sending" do
    handshake = Augmented::HandshakeRequest.new
    handshake.sending = true
    expect(handshake.sending?).to be(true)
  end

  context "token" do
    it "is unique across contacts" do
      contact = Augmented::Contact.create(name: "test", url: "test.com", token: "takentoken")
      handshake = Augmented::HandshakeRequest.new(name: "thing", url: "site.web.com", token: "takentoken")
      expect(handshake.valid?).to be(false)
      expect(handshake.errors[:token]).to eq(["is already taken"])
    end

    it "is unique across handshake requests" do
      contact = Augmented::HandshakeRequest.create(name: "test", url: "test.com", token: "takentoken")
      handshake = Augmented::HandshakeRequest.new(name: "thing", url: "site.web.com", token: "takentoken")
      expect(handshake.valid?).to be(false)
      expect(handshake.errors[:token]).to eq(["is already taken"])
    end
  end

  context "generate_sending_request" do
    it "returns a handshake request with a generated token" do
      handshake = Augmented::HandshakeRequest.generate_sending_request(url: "test.com", name: "steve")
      expect(handshake.token).to_not be_nil
    end

    it "flags the handshake request as sending" do
      handshake = Augmented::HandshakeRequest.generate_sending_request(url: "test.com", name: "steve")
      expect(handshake.sending).to eq(true)
      expect(handshake.sending).to be(true)
    end
  end

  context "generate_token" do
    it "generates a random UUID token" do
      expect(Augmented::HandshakeRequest.generate_token).to_not be_nil
    end
  end

  context "send_handshake_request!" do
    it "does nothing if handshake request is not a sending request" do
      handshake = Augmented::HandshakeRequest.new(name: "thing", url: "site.web.com", token: "takentoken")
      expect(handshake.send_handshake_request!).to be_nil
    end

    it "sends a http request to the url" do
      UUID.any_instance.stub(:generate).and_return("123abcF")
      handshake = Augmented::HandshakeRequest.generate_sending_request(url: "test.com", name: "steve dude")
      handshake_request_success_stub("test.com")
      handshake.send_handshake_request!
      expect(WebMock).to have_requested(:post, "test.com/handshake_request.json").with(body: {"name" => Augmented::CONFIG.name, "url" => Augmented::CONFIG.url, "token" => "123abcF"})
    end

    context "when response is an error" do
      it "sets failed_at to current time" do
        handshake_request_error_stub("test.com")
        handshake = Augmented::HandshakeRequest.generate_sending_request(:url => "test.com", :name => "steve dude")
        handshake.send_handshake_request!
        expect(handshake.failed_at).to_not be_nil
      end
    end
  end

  context "send_handshake_approval!" do
    it "sends a http request to the url" do
      handshake = Augmented::HandshakeRequest.create(:url => "test.com", :name => "steve dude", :token => "test123a")
      handshake_approval_success_stub("test.com")
      handshake.send_handshake_approval!
      expect(WebMock).to have_requested(:post, "test.com/handshake_approval.json").with(:body => {"token" => "test123a"})
    end

    context "when response is handshake approved" do
      it "sets approved_at to current time" do
        handshake_approval_success_stub("test.com")
        Timecop.freeze(Time.local(2012, 10, 31)) do
          handshake = Augmented::HandshakeRequest.create(:url => "test.com", :name => "apple", :token => "foowho")
          handshake.send_handshake_approval!
          expect(handshake.approved_at).to eq(Time.now)
        end
      end
    end

    context "when response is an error" do
      it "does not set approved_at" do
        handshake_approval_error_stub("test.com")
        handshake = Augmented::HandshakeRequest.create(:url => "test.com", :name => "steve dude", :token => "123")
        handshake.send_handshake_request!
        expect(handshake.approved_at).to be_nil
      end
    end
  end

  context "all_for_landing_page" do
    before do
      @h1 = Augmented::HandshakeRequest.create(:url => "test.com", :name => "Handshake 1", :token => "token1")
      @h2 = Augmented::HandshakeRequest.create(:url => "apple.com", :name => "Handshake 2", :token => "token2", :failed_at => Time.now, :sending => true)
      @h3 = Augmented::HandshakeRequest.create(:url => "foobar.com", :name => "Handshake 3", :token => "token3", :sending => true)
    end

    it "should return handshake requests that need approval" do
      expect(Augmented::HandshakeRequest.all_for_landing_page).to include(@h1)
    end

    it "should return handshake requests that can be resent" do
      expect(Augmented::HandshakeRequest.all_for_landing_page).to include(@h2)
    end

    it "should not return sending requests awaiting approval" do
      expect(Augmented::HandshakeRequest.all_for_landing_page).to_not include(@h3)
    end
  end
end
