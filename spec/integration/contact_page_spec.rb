require_relative './integration_helper.rb'

describe "contact page", type: :feature do
  before do
    @contact = Augmented::Contact.create(token: "1234abc", name: "Roberto", url: "test.com")
  end
  
  it "shows the contact" do
    visit "/contact/#{@contact.id}"
    page.should have_content("Roberto")
    page.should have_content("test.com")
    page.should have_content("1234abc")
  end
end
