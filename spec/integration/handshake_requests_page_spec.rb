require_relative './integration_helper.rb'

def fill_in_and_submit_handshake_request(name, url)
  fill_in "name", with: name
  fill_in "url", with: url
  click_button "Send Handshake Request"
end

describe "contacts page", type: :feature do
  before do
    Augmented::HandshakeRequest.create(name: "Dave", token: "token123", url: "aurl.com")
    Augmented::HandshakeRequest.create(name: "Steve", token: "T1235", url: "test.com", sending: true)
    visit "/handshake_requests"
  end
  
  it "shows all handshake requests" do
    Augmented::HandshakeRequest.all.each do |handshake|
      page.should have_content(handshake.name)
    end
  end
  
  describe "handshake request creation" do
    it "lets you send a handshake request" do
      handshake_request_success_stub("localhost")

      fill_in_and_submit_handshake_request("Steve", "localhost")

      page.should have_content("Handshake Request sent to Steve")
    end

    it "displays errors for failed handshake request" do
      handshake_request_error_stub("localhost")

      fill_in_and_submit_handshake_request("Steve", "localhost")

      page.should have_content("Handshake Request failed to send to Steve: unable to create handshake request")
    end
  end
end
