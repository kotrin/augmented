require_relative './integration_helper.rb'

describe "contacts page", type: :feature do
  before do
    Augmented::Contact.create(token: "1234abc", name: "Roberto", url: "test.com")
    Augmented::Contact.create(token: "jjgg", name: "Steve", url: "test123.com")
    visit "/contacts"
  end
  
  it "shows all contacts" do
    Augmented::Contact.all.each do |contact|
      page.should have_content(contact.token)
      page.should have_content(contact.name)
      page.should have_content(contact.url)
    end
  end
  
  describe "create a contact" do
    it "lets you create a new contact" do
      fill_in "name", with: "burtz"
      fill_in "url", with: "localhost:3000"
      fill_in "token", with: "token123"
      click_button "Create Contact"
      
      page.should have_content("burtz")
      page.should have_content("localhost:3000")
      page.should have_content("token123")
      
      page.should have_content("Contact created")
    end
  end
end
