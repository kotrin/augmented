require_relative './integration_helper.rb'

def fill_in_and_submit_message(msg)
  fill_in "message", with: msg
  click_on "send-message"
end

describe "landing page", type: :feature do
  before do
    @contact  = Augmented::Contact.create(token: "1234abc", name: "Roberto", url: "test.com")
    message   = Augmented::Message.create(contact_id: @contact.id, body: "apple!")
    message2  = Augmented::Message.create(contact_id: @contact.id, body: "stuff sauce!", read_at: Time.now)
    message3  = Augmented::Message.create(contact_id: @contact.id, body: "yeah mang", sending: true)
    visit '/'
  end
  
  describe "message area" do
    before do
      message_success_stub("test.com")
    end

    it "displays messages FROM the contact" do
      within("#messages") do
        page.should have_content("apple!")
      end
    end

    it "displays messages TO the contact" do
      within("#messages") do
        page.should have_content("yeah mang")
      end
    end

    it "lets you send a message to the contact" do
      fill_in_and_submit_message("hello!")
      page.should have_content("Message sent to Roberto")
      page.should have_content("hello!")
    end

    context "when contact server is down" do
      before do
        message_error_stub("test.com")
        fill_in_and_submit_message("hi!")
      end

      it "displays error for failed message" do
        current_path.should == "/#{@contact.id}"
        page.should have_content("Message failed to send to #{@contact.name}: unable to save message. ensure message format is correct")
      end

      it "lets you resend failed messages" do
        page.should have_content("Resend")
        message_success_stub("test.com")
        
        within("#messages") do
          click_link "Resend"
        end
        
        within("#messages") do
          page.should_not have_content("Resend")
        end
        
        page.should have_content("Message sent to")
      end
    end
  end

end
