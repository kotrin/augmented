require_relative './spec_helper'

describe Augmented::ApiRequest do
  context "send" do
    it "sends a payload to a url via HTTP POST" do
      stub_request(:post, 'test.com/test.json').to_return(body: {created: 'yay'}.to_json)
      Augmented::ApiRequest.new('http://test.com/test.json', token: "1234")
      WebMock.should have_requested(:post, "test.com/test.json").with(body: {"token" => "1234"})
    end
    
    context "with unsuccessful delivery" do
      it "responds with errors" do
        stub_request(:post, 'test.com/test.json').to_return(body: {created: 'yay'}.to_json)
        Net::HTTP.any_instance.stub(:start).and_return(false)
        request = Augmented::ApiRequest.new('http://test.com/test.json', token: "1234")
        request.response.should == { success: false, errors: 'failure talking to server' }
        Net::HTTP::Post.any_instance.stub(:new).and_raise(Exception)
        request = Augmented::ApiRequest.new('http://test.com/test.json', token: "1234")
        request.response.should == { success: false, errors: 'failure talking to server' }
      end
    end
  end
end
