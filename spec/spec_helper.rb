require 'bundler'

Bundler.require(:default, :test)

require 'rack/test'
require 'webmock/rspec'

require 'simplecov'
SimpleCov.start

# memory db
DB = Sequel.sqlite
Sequel.extension :migration
Sequel::Migrator.apply(DB, 'db/migrations')

RSpec.configure do |c|
  c.around(:each) do |example|
    DB.transaction(rollback: :always){example.run}
  end
end

require_relative '../lib/augmented.rb'

def handshake_approval_success_stub(url)
  stub_request(:post, url + "/handshake_approval.json")
    .to_return(body: {handshake: 'approved'}.to_json)
end

def handshake_approval_error_stub(url)
  stub_request(:post, url + "/handshake_approval.json")
    .to_return(body: {error: 'error approving handshake'}.to_json)
end

def handshake_request_success_stub(url)
  stub_request(:post, url + "/handshake_request.json")
    .to_return(body: {handshake: 'created'}.to_json)
end

def handshake_request_error_stub(url)
  stub_request(:post, url + "/handshake_request.json")
    .to_return(body: {error: 'unable to create handshake request'}.to_json)
end

def message_success_stub(url)
  stub_request(:post, url + "/message.json")
    .to_return(body: {message: 'created' }.to_json)
end

def message_error_stub(url)
  stub_request(:post, url + "/message.json")
    .to_return(body: {error: 'unable to save message. ensure message format is correct'}.to_json)
end