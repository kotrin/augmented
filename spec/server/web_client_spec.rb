require_relative './../spec_helper'

describe Augmented::Server::WebClient do
  include Rack::Test::Methods
  
  def app
    @app ||= Augmented::Server::WebClient.new
  end
  
  context "GET /style.css" do
    it "responds successfully" do
      get "/style.css"
      last_response.should be_successful
    end
  end
  
  context "GET /contacts" do
    it "responds successfully" do
      get "/contacts"
      last_response.should be_successful
    end
  end
  
  context "GET /handshake_requests" do
    it "responds successfully" do
      get "/handshake_requests"
      last_response.should be_successful
    end
  end

  context "GET /" do
    it "responds successfully" do
      get '/'
      last_response.should be_successful
    end
  end

  context "GET /handshake_request/:id/approve" do
    before do
      @handshake = Augmented::HandshakeRequest.create(:token => "test1234a", :name => "Roberto A", :url => "testurl.com")
      @approval_url = "/handshake_request/#{@handshake.id}/approve"
      handshake_approval_success_stub("testurl.com")
    end

    it "sends a handshake request approval to the handshake url" do
      get @approval_url
      WebMock.should have_requested(:post, "testurl.com/handshake_approval.json").with(:body => {"token" => "test1234a"})
    end

    it "redirects to the landing page" do
      get @approval_url
      last_response.should be_redirect
    end

    it "creates a contact with successful approval response" do
      lambda {
        get @approval_url
      }.should change(Augmented::Contact, :count).from(0).to(1)
    end

    context "when the handshake request approval fails" do
      it "redirects back to the landing page" do
        handshake_approval_error_stub("testurl.com")
        get @approval_url
        last_response.should be_redirect
      end
    
      it "creates no contact" do
        handshake_approval_error_stub("testurl.com")
        lambda {
          get @approval_url
        }.should_not change(Augmented::Contact, :count)
      end
    end
  end

  context "GET /handshake_request/:id/resend" do
    before do
      @handshake = Augmented::HandshakeRequest.create(token: "test1234a", name: "Roberto A", url: "testurl.com", sending: true, failed_at: Time.now)
      @resend_url = "/handshake_request/#{@handshake.id}/resend"
      handshake_request_success_stub("testurl.com")
    end

    it "sends a handshake request to the url" do
      get @resend_url
      WebMock.should have_requested(:post, "testurl.com/handshake_request.json").with(:body => {"name" => Augmented::CONFIG.name, "url" => Augmented::CONFIG.url, "token" => "test1234a"})
    end

    it "redirects to the landing page" do
      get @resend_url
      last_response.should be_redirect
    end

    it "clears the handshakes failed_at" do
      get @resend_url
      @handshake.reload.failed_at.should be_nil
    end
  end
  
  context "GET /handshake_request/:id/delete" do
    it "deletes handshake request" do
      handshake = Augmented::HandshakeRequest.create(token: "wee3", name: "Dave", url: "applefaces.com")
      id = handshake.id
      get "/handshake_request/#{id}/delete"
      Augmented::HandshakeRequest[id].should be_nil
    end
    
    it "returns error for invalid handshake request" do
      lambda {
        get "/handshake_request/3151351/delete"
      }.should change(Augmented::HandshakeRequest, :count).by(0)
    end
  end

  context "POST /handshake_request" do
    context "with valid parameters" do
      it "creates a handshake request" do
        handshake_request_success_stub("test.com")
        lambda {
          post '/handshake_request', "name" => "Steve Man", "url" => "test.com"
        }.should change(Augmented::HandshakeRequest, :count).from(0).to(1)
      end

      it "sends a handshake request to the url" do
        UUID.any_instance.stub(:generate).and_return("123ABCd")
        handshake_request_success_stub("test.com")
        post '/handshake_request', "name" => "Steve Man", "url" => "test.com"
        WebMock.should have_requested(:post, "test.com/handshake_request.json").with(:body => {"name" => Augmented::CONFIG.name, "url" => Augmented::CONFIG.url, "token" => "123ABCd"})
      end

      it "redirects back to the landing page" do
        handshake_request_success_stub("test.com")
        post '/handshake_request', "name" => "Steve Man", "url" => "test.com"
        last_response.should be_redirect
      end

      context "when the handshake request http post fails" do
        it "redirects back to the landing page" do
          handshake_request_error_stub("test.com")
          post '/handshake_request', "name" => "Steve Man", "url" => "test.com"
          last_response.should be_redirect
        end

        it "sets the handshake requests failed_at flag" do
          Timecop.freeze(Time.local(2012, 10, 10)) do
            handshake_request_error_stub("test.com")
            post '/handshake_request', "name" => "Test Time", "url" => "test.com"
            handshake = Augmented::HandshakeRequest.where(:name => "Test Time", :url => "test.com").first
            handshake.failed_at.should == Time.now
          end
        end
      end
    end

    context "with invalid parameters" do
      it "redirects back to the landing page without creating a handshake" do
        lambda {
          post '/handshake_request', "name" => "Steve Man"
        }.should_not change(Augmented::HandshakeRequest, :count)
        last_response.should be_redirect
      end
    end
  end
  
  context 'GET /contact/:id' do
    it 'responds successfully' do
      contact = Augmented::Contact.create(token: 'fu', name: 'brt', url: 'localhost')
      get "/contact/#{contact.id}"
      last_response.should be_successful
    end
  end
  
  context 'GET /contact/:id/messages' do
    it 'returns json array of unread contact messages' do
      contact = Augmented::Contact.create(token: 'token', name: 'brtz', url: 'localhost')
      message1 = Augmented::Message.create(contact_id: contact.id, body: 'hi1')
      message2 = Augmented::Message.create(contact_id: contact.id, body: 'hi2')
      message3 = Augmented::Message.create(contact_id: contact.id, body: 'hi3', read_at: Time.now)
      
      get "/contact/#{contact.id}/messages"
      
      response = JSON.generate(['hi1', 'hi2'])
      last_response.body.should eq(response)
    end
  end
  
  context "POST /contact" do
    it "creates a contact" do
      -> {
        post '/contact', name: 'burt', token: 'token123', url: 'localhost'
      }.should change(Augmented::Contact, :count).from(0).to(1)
    end
  end

  context "POST /contact/:id/message" do
    before do
      @contact = Augmented::Contact.create(:token => "test123", :name => "Robert", :url => "urltest.com")
    end

    context "with a valid message" do
      before do
        message_success_stub("urltest.com")
      end

      it "creates a message" do
        lambda {
          post "contact/#{@contact.id}/message", "message" => "hola!"
        }.should change(Augmented::Message, :count).from(0).to(1)
      end

      it "sends a message to the contact url" do
        post "contact/#{@contact.id}/message", "message" => "hola!"
        WebMock.should have_requested(:post, "urltest.com/message.json").with(:body => { :token => "test123", :body => "hola!" })
      end

      it "redirects back to the contact page" do
        post "contact/#{@contact.id}/message", "message" => "hola!"
        last_response.should be_redirect
      end

      context "when the message http post fails" do
        it "redirects back to the contact page" do
          message_error_stub("urltest.com")
          post "contact/#{@contact.id}/message", "message" => "hola!"
          last_response.should be_redirect
        end

        it "sets the message failed_at flag" do
          Timecop.freeze(Time.local(2012, 10, 10)) do
            message_error_stub("urltest.com")
            post "contact/#{@contact.id}/message", "message" => "hola!"
            message = Augmented::Message.where(:contact_id => @contact.id, :body => "hola!").first
            message.failed_at.should == Time.now
          end
        end
      end
    end

    context "with an invalid message" do
      it "redirects back to the contact page with no message made" do
        lambda {
          post "/contact/#{@contact.id}/message", "message" => ""
        }.should_not change(Augmented::Message, :count)
        last_response.should be_redirect
      end
    end
  end

  context "GET /contact/:contact_id/message/:id/resend" do
    before do
      contact = Augmented::Contact.create(:name => "Dave", :token => "test1234a", :url => "testurl.com")
      @message = Augmented::Message.create(:contact_id => contact.id, :body => "sup!", :failed_at => Time.now, :sending => true)
      @resend_url = "/contact/#{contact.id}/message/#{@message.id}/resend"
      message_success_stub("testurl.com")
    end

    it "sends a handshake request to the url" do
      get @resend_url
      WebMock.should have_requested(:post, "testurl.com/message.json").with(:body => {"body" => "sup!", "token" => "test1234a"})
    end

    it "redirects to the landing page" do
      get @resend_url
      last_response.should be_redirect
    end

    it "clears the handshakes failed_at" do
      get @resend_url
      @message.reload.failed_at.should be_nil
    end
  end
end