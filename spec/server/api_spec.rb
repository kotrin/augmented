require_relative './../spec_helper'

describe Augmented::Server::Api do
  include Rack::Test::Methods
  
  def app
    @app ||= Augmented::Server::Api.new
  end
  
  context "POST message.json" do
    it "returns a 400 if token can not be matched" do
      post '/message.json', "token" => "1234", "body" => "test"
      last_response.status.should == 400
    end

    it "creates a message if token is matched" do
      contact = Augmented::Contact.create(:name => "Test", :token => "12345")
      lambda {
        post '/message.json', "token" => "12345", "body" => "testing!"
      }.should change(Augmented::Message.where(:contact_id => contact.id), :count).from(0).to(1)
      last_response.should be_successful
      last_response.body.should == '{"message":"created"}'
    end

    it "returns an error if body is missing" do
      Augmented::Contact.create(:name => "Test", :token => "12345")
      post '/message.json', "token" => "12345"
      last_response.should be_successful
      last_response.body.should == '{"error":"unable to save message. ensure message format is correct"}'
    end
  end

  context "POST handshake_request.json" do
    it "creates a handshake request if given properly formatted handshake" do
      lambda {
        post '/handshake_request.json', "token" => "rand0m", "name" => "roberto", "url" => "www.test.com"
      }.should change(Augmented::HandshakeRequest, :count).from(0).to(1)
      last_response.should be_successful
      last_response.body.should == '{"handshake":"created"}'
    end

    it "returns an error if handshake request is invalid" do
      post '/handshake_request.json', "token" => "rand0m", "name" => "roberto"
      last_response.should be_successful
      last_response.body.should == '{"error":"unable to create handshake request"}'
    end

    it "returns a token invalid if token is already taken by a contact" do
      Augmented::Contact.create(:name => "test", :token => "rand0m", "url" => "aurl.com")
      post '/handshake_request.json', "token" => "rand0m", "name" => "roberto", "url" => "test.com"
      last_response.should be_successful
      last_response.body.should == '{"error":"invalid token"}'
    end
  end

  context "POST handshake_approval.json" do
    it "approves a handshake request if given token is a handshake request waiting for approval" do
      handshake = Augmented::HandshakeRequest.create(:token => "testt0ken", "name" => "robert", "url" => "www.test.com")
      handshake.approved_at.should == nil
      post '/handshake_approval.json', "token" => "testt0ken"
      handshake.reload.approved_at.should_not == nil
      last_response.should be_successful
    end

    it "returns an error if token is not waiting approval" do
      post '/handshake_approval.json', "token" => "testt0ken1"
      last_response.status.should == 400
    end

    it "creates a contact with handshake requests data on approval" do
      handshake = Augmented::HandshakeRequest.create(:token => "testt0ken", "name" => "robert", "url" => "www.test.com")
      lambda {
        post '/handshake_approval.json', "token" => "testt0ken"
      }.should change(Augmented::Contact, :count).from(0).to(1)
      last_response.should be_successful
    end
  end
end