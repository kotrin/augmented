# Augmented

This is my attempt to make a neat web based communications something.

## What you need

  * Ruby 1.9+
  * SQLite3
  
Install gems

      bundle install

## See stuff

Get [augmented-echo](https://github.com/kotrin/augmented-echo). Run augmented-echo by following the projects README.

Run Augmented on port 3000 (default config for augmented-echo):

     bundle exec rackup -p 3000

Go to `http://localhost:3000/` and send a handshake request to augmented-echo via URL `localhost:3333`

## Do other stuff

Run the tests

    bundle exec rspec