module Augmented
  module Server
    class Api < Sinatra::Base
      helpers Sinatra::JSON
      
      post '/message.json' do
        contact = Augmented::Contact.where(token: params[:token]).first
        if contact
          message = Augmented::Message.new(contact_id: contact.id, body: params[:body])
          if message.valid? && message.save
            json message: 'created'
          else
            json error: 'unable to save message. ensure message format is correct'
          end
        else
          status 400
        end
      end

      post '/handshake_request.json' do
        handshake = Augmented::HandshakeRequest.new(token: params[:token], name: params[:name], url: params[:url])
        if handshake.valid? && handshake.save
          json handshake: 'created'
        else
          token_errors = handshake.errors[:token]
          if token_errors.nil? || token_errors.empty?
            json error: 'unable to create handshake request'
          else
            json error: 'invalid token'
          end
        end
      end

      post '/handshake_approval.json' do
        handshake = Augmented::HandshakeRequest.where(:approved_at => nil, :token => params[:token]).first
        if handshake
          handshake.approved_at = Time.now
          if handshake.save
            Augmented::Contact.create(:name => handshake.name, :url => handshake.url, :token => handshake.token)
            json handshake: 'approved'
          else
            json error: 'error approving handshake'
          end
        else
          status 400
        end
      end
    end
  end
end