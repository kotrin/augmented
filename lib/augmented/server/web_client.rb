module Augmented
  module Server
    class WebClient < Sinatra::Base
      enable :sessions
      
      use Rack::Flash
      
      helpers Sinatra::JSON
      
      before do
        @handshake_request_count = Augmented::HandshakeRequest.where(sending: false, approved_at: nil).count
      end
      
      helpers do
        def li_current(path)
          if request.path_info == path
            "<li class='active'>"
          else
            "<li>"
          end
        end
      end
      
      get '/handshake_requests' do
        @handshake_requests = Augmented::HandshakeRequest.all
        erb :handshake_requests
      end
      
      get '/contacts' do
        @contacts = Augmented::Contact.all
        erb :contacts
      end
      
      get '/:contact_id?' do
        @contacts = Augmented::Contact.all
        if params[:contact_id]
          @contact = Augmented::Contact[params[:contact_id]]
        else
          @contact = @contacts.first if @contacts.size > 0
        end
        erb :index
      end

      get '/handshake_request/:id/approve' do
        handshake = Augmented::HandshakeRequest[params[:id]]
        response = handshake.send_handshake_approval!
        if response[:success]
          Augmented::Contact.create(name: handshake.name, url: handshake.url, token: handshake.token)
          flash[:notice] = "Handshake approved! #{handshake.name} has been added to your contacts"
        else
          flash[:notice] = "Handshake approval failed: #{response[:errors]}"
        end
        redirect '/handshake_requests'
      end

      post '/handshake_request' do
        handshake = Augmented::HandshakeRequest.generate_sending_request(name: params[:name], url: params[:url])
        if handshake.valid? && handshake.save
          send_handshake_request(handshake)
        end
        redirect '/handshake_requests'
      end

      get '/handshake_request/:id/resend' do
        handshake = Augmented::HandshakeRequest[params[:id]]
        send_handshake_request(handshake)
        redirect '/handshake_requests'
      end
      
      get '/handshake_request/:id/delete' do
        handshake = Augmented::HandshakeRequest[params[:id]]
        if handshake && handshake.delete
          flash[:notice] = "Handshake request deleted"
        else
          flash[:notice] = "Failed to delete handshake request"
        end
        redirect '/handshake_requests'
      end

      get '/contact/:id' do
        @contact = Augmented::Contact.where[params[:id]]
        erb :contact
      end
      
      get '/contact/:id/messages' do
        contact = Augmented::Contact[id: params[:id]]
        response = []
        if contact
          unread_messages = contact.messages_dataset.where(read_at: nil)
          unread_messages.all.each do |message|
            message.mark_read!
            response << message.formatted_body
          end
        end
        json response
      end
      
      post '/contact' do
        opts = {
          token: params[:token],
          name: params[:name],
          url: params[:url]
        }
        contact = Augmented::Contact.new(opts)
        if contact.valid? && contact.save
          flash[:notice] = 'Contact created'
        else
          flash[:notice] = 'Invalid contact'
        end
        redirect '/contacts'
      end

      post '/contact/:id/message' do
        contact = Augmented::Contact[id: params[:id]]
        message = Augmented::Message.new(contact_id: contact.id, body: params[:message], sending: true)
        if message.valid? && message.save
          send_message(message)
        else
          flash[:notice] = "Invalid message"
        end
        redirect "/#{contact.id}"
      end

      get '/contact/:contact_id/message/:id/resend' do
        contact  = Augmented::Contact[params[:contact_id]]
        message  = Augmented::Message.where(contact_id: contact.id, id: params[:id], sending: true).first
        send_message(message)
        redirect "/#{contact.id}"
      end
      
      private

      def send_message(message)
        response = message.send_message!
        contact  = message.contact

        if response[:success]
          flash[:notice] = "Message sent to #{contact.name}"
        else
          flash[:notice] = "Message failed to send to #{contact.name}: #{response[:errors]}"
        end
      end

      def send_handshake_request(handshake)
        response = handshake.send_handshake_request!
        if response[:success]
          flash[:notice] = "Handshake Request sent to #{handshake.name}"
        else
          flash[:notice] = "Handshake Request failed to send to #{handshake.name}: #{response[:errors]}"
        end
      end
    end
  end
end