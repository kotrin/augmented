require 'uuid'
require 'net/http'
require 'json'

module Augmented
  class HandshakeRequest < Sequel::Model
    plugin :validation_helpers

    def validate
      super
      validates_presence [:name, :url, :token]
      validates_unique_token
    end

    def self.generate_sending_request(opts = {})
      name  = opts.delete(:name)
      url   = opts.delete(:url)
      token = self.generate_token
      self.new(token: token, name: name, url: url, sending: true)
    end

    def self.generate_token
      UUID.generate
    end

    def self.all_for_landing_page
      approval = self.where(approved_at: nil, sending: false).all
      resend   = self.exclude(failed_at: nil).where(sending: true).all 
      approval + resend
    end

    def sending?
      self.sending
    end
    
    def status
      if sending?
        if failed_at.nil?
          if approved_at.nil?
            'Awaiting approval'
          else
            'Approved'
          end
        else
          'Failed'
        end
      else
        if approved_at.nil?
          'Awaiting approval'
        else
          'Approved'
        end
      end
    end

    def send_handshake_request!
      if sending?
        request_url = url_for('handshake_request.json')
        opts = {
          name: Augmented::CONFIG.name, 
          url: Augmented::CONFIG.url, 
          token: self.token
        }
        request = Augmented::ApiRequest.new(request_url, opts)

        if request.response[:success]
          self.failed_at = nil
        else
          self.failed_at = Time.now
        end
        self.save

        request.response
      end
    end

    def send_handshake_approval!
      request_url = url_for('handshake_approval.json')
      request = Augmented::ApiRequest.new(request_url, token: self.token)

      if request.response[:success]
        self.approved_at = Time.now
      else
        self.approved_at = nil
      end
      self.save

      request.response
    end

    private
    
    # TODO:
    # sanitize url for strip trailing '/' and to ensure http(s):// during input

    def url_for(site)
      'http://' + self.url + '/' + site 
    end

    def validates_unique_token
      validates_unique :token

      if Augmented::Contact.where(token: token).count > 0
        errors.add(:token, 'is already taken')
      end
    end
  end
end
