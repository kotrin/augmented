module Augmented
  class ApiRequest
    attr_reader :response

    def initialize(url, payload = {})
      response  = send_payload(url, payload)
      @response = parse_response(response)
      Augmented.logger("[RESPONSE] #{@response}")
    end

    private

    def send_payload(url, payload)
      Augmented.logger("[PAYLOAD] #{payload}")

      uri = URI(url)
      req = Net::HTTP::Post.new(uri.path)
      req.set_form_data(payload)

      Augmented.logger("[POST] #{url}")

      res = Net::HTTP.start(uri.host, uri.port) do |http|
        http.request(req)
      end

      Augmented.logger("[RESPONSE] #{res}")

      case res
      when Net::HTTPSuccess
        res
      else
        false
      end

    rescue => e
      Augmented.logger("[ERROR] #{e}")
      false
    end

    def parse_response(res)
      Augmented.logger("[PARSE] #{res}")
      if res
        json_body = JSON.parse(res.body)
        if json_body['error']
          response_with_errors(json_body['error'])
        else
          response_with_success
        end
      else
        response_with_errors('failure talking to server')
      end
    end
    
    def response_with_errors(errors)
      { success: false, errors: errors }
    end
    
    def response_with_success
      { success: true, errors: '' }
    end
  end
end
