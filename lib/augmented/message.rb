require 'twitter-text'

class TwitterFormatting
  include Twitter::Autolink
end

module Augmented
  class Message < Sequel::Model
    plugin :validation_helpers

    many_to_one :contact

    def validate
      super
      validates_presence [:body]
    end

    def sending?
      self.sending
    end
    
    def mark_read!
      if self.read_at.nil?
        self.read_at = Time.now
        self.save
      end
    end
    
    def send_message!
      if sending?
        request_url = url_for('message.json')
        opts = {
          token: self.contact.token, 
          body: self.body
        }
        request = Augmented::ApiRequest.new(request_url, opts)

        if request.response[:success]
          self.failed_at = nil
        else
          self.failed_at = Time.now
        end
        self.save

        request.response
      end
    end

    def formatted_body
      text = self.body
      text.gsub!('<') { "&lt;" }
      text.gsub!('>') { "&gt;" }
      TwitterFormatting.new.auto_link_urls(text)
    end

    private

    def url_for(site)
      'http://' + self.contact.url + '/' + site 
    end
  end
end
