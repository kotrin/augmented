module Augmented
  class Contact < Sequel::Model
    plugin :validation_helpers

    one_to_many :messages

    def validate
      super
      validates_presence [:name, :token]
    end
    
    def unread_messages_count
      Augmented::Message.where(read_at: nil, contact_id: self.id, sending: false).count
    end
  end
end
