require 'sinatra/base'
require 'sinatra/json'
require 'rack/flash'
require 'sequel'
require 'erb'

module Augmented
  CONFIG = OpenStruct.new(YAML.load(ERB.new(File.read("config/config.yml")).result))
  
  def self.logger(s)
    if CONFIG.debug || ENV['DEBUG']
      puts "DEBUG: " + s
    end
  end
end

require_relative 'augmented/api_request.rb'
require_relative 'augmented/message.rb'
require_relative 'augmented/contact.rb'
require_relative 'augmented/handshake_request.rb'
require_relative 'augmented/server/api.rb'
require_relative 'augmented/server/web_client.rb'
