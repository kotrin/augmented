require 'rubygems'
require 'bundler'

Bundler.require

# memory db
DB = Sequel.sqlite
Sequel.extension :migration
Sequel::Migrator.apply(DB, 'db/migrations')

require './lib/augmented'

run Rack::URLMap.new("/"    => Augmented::Server::WebClient.new, 
                     "/api" => Augmented::Server::Api.new)